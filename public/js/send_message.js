function send_message(event) {
  event.preventDefault();

  // Form data
  const name = document.getElementById("name").value;
  const mail = document.getElementById("mail").value;
  const phone = document.getElementById("phone").value;
  const company = document.getElementById("company").value;
  const mail_message =  document.getElementById("message").value;
  //const adr_csrf =
  //console.log(window.csrf)
  // Attaching the values to form data.
  const formData = new FormData();
  formData.append("name", name);
  formData.append("mail", mail);
  formData.append("phone", phone);
  formData.append("company", company);
  formData.append("mail_message", mail_message);

  console.log("CSRF:", document.getElementsByName('csrf-token')[0].getAttribute('content'))
  const requestOptions = {
    method: "POST",
    body: formData,
    //headers: {"x-csrf-token": window.csrf}
    //headers: {"x-csrf-token": $('meta[name="csrf-token"]').attr('content')}
    headers: {"x-csrf-token": document.getElementsByName('csrf-token')[0].getAttribute('content')}
  }

  //fetch('/mail', requestOptions).then(res => console.log(res));
  fetch('/mail', requestOptions).then((res) => {
    swal({
      title: "Success",
      icon: "success",
      text: "Message sent!"
    });
  }).catch(() => {
    swal({
      title: "Error",
      icon: "error",
      text: "Message not sent!"
    });
  });

}


/*function send_message(event) {
  event.preventDefault();
  const name = document.getElementById("name").value;
  const mail = document.getElementById("mail").value;
  const phone = document.getElementById("phone").value;
  const company = document.getElementById("company").value;
  const mail_message =  document.getElementById("message").value;
  //var formName = $("#formName").val()
  //var formEmail = $("#formEmail").val()
  //var formTel = $("#formTel").val()
  //var formMessage = $("#formMessage").val()
  // var formPrice = $("#formPrice").val()
  // var formLink = $("#formLink").val()
  // console.log(name);


  $.ajax({
    type: 'POST',
    url: '/mail',
    data:
      {
        name: name, mail: mail, phone: phone, company: company, mail_message: mail_message
      },
    success: function (response) {
      //$('.form__body')[0].reset();

      swal({
        title: "{{ __('tr.js_subs_success') }}",
        icon: "success",
        text: "{{ __('tr.js_subs_success_message') }}"
      });

    },
    error: function (response) {
      swal({
        title: "{{ __('tr.js_send_error') }}",
        icon: "error",
        text: "{{ __('tr.js_error_msg') }}"
      });
    }
  });

}


*/

<div class="main-sliders">
  <div class="main-sliders__wrapper">
    <div class="main-sliders__sliders _swiper">
      @foreach($top_sliders as $top_slider)
        <div class="main-sliders__slide _ibg">
          <img class="main-sliders__bg" src="{{asset(Voyager::image($top_slider->background_image))}}" alt="">
          <div class="main-sliders__content _container">
            <div class="main-sliders__row">
              <div class="main-sliders__column">
                <div class="main-sliders__descr text-label">{{$top_slider->getTranslatedAttribute('desc')}}
                </div>
                <div class="main-sliders__title">{{$top_slider->getTranslatedAttribute('title')}}
                </div>
                <span class="main-sliders__text text-block">
                  {{$top_slider->getTranslatedAttribute('text')}}
                </span>
                <a href="{{$top_slider->getTranslatedAttribute('link')}}" class="main-sliders__link btn-block">
                  {{$top_slider->getTranslatedAttribute('link_text')}}
                </a>
              </div>
              <div class="main-sliders__column">
                <div class="main-sliders__image">
                  <img  src="{{asset(Voyager::image($top_slider->right_image))}}" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    {{-- Arrows and gif --}}
    <div class="main-sliders__left"></div>
    <div class="main-sliders__right"></div>
    <div class="main-sliders__gif">
      <picture>
        <source srcset="{{asset('./img/right.gif')}}" type="image/webp">
        <img src="{{asset('./img/right.gif')}}" alt=""></picture>
    </div>
  </div>
</div>

<head>
  <meta charset="UTF-8">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name='csrf-token' content="{{ csrf_token() }}">
  <meta name="keywords" content="{!! $page-> getTranslatedAttribute('meta_keywords') !!}"/>
  <meta name="description" content="{!! $page-> getTranslatedAttribute('meta_descriptions') !!}"/>
  <meta property="og:title" content="{!! $page->getTranslatedAttribute('seo_title') !!}">
  <meta property="og:description" content="{!! $page-> getTranslatedAttribute('meta_descriptions') !!}">
  <title>{!! $page->getTranslatedAttribute('seo_title') !!}</title>
  <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
  <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
  <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
</head>

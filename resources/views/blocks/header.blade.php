<!-- Header -->
<header class="header">
  <div class="header__content _container">
    <div data-da="header__nav,2,767" class="header__top top-header">
      <div class="top-header__contacts contacts">
        <ul class="contacts__list">
          @foreach($header_links as $link)
            <li>{{--<i class="{{$link->icon}}"></i>--}}<a href="{{$link->link}}" class="contacts__item {{$link->icon}}">{{$link->getTranslatedAttribute('text')}}</a></li>
          @endforeach
        </ul>
      </div>
      <div class="top-header__adress">
        <span class="contacts__item contacts__item_nomg">
          @foreach($header_texts as $text)
            @if($text->identifier == "header_address")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </span>
      </div>
    </div>
    <div class="header__bottom">
      <a href="/" class="header__logo">
        <img src="{{asset(Voyager::image($header_logo->value))}}" alt="">
      </a>
      <div class="hedear__menu">
        <div class="icon-menu">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <nav class="header__nav nav">
          <ul class="nav__list">
            @foreach($menu_constructor as $item)
              <li class="nav__li"><a href="/{{app()->getLocale()}}/{{$item->menu_slug}}" class="nav__link">{{$item->title}}</a></li>
            @endforeach
            <li class="nav__li nav__li_lg"><a href="{{str_replace(url('/') . '/' .  app()->getLocale(), url('/') . '/' .'ru', request()->fullUrl())}}" class="nav__link">
                <picture>
                  <source srcset="{{asset('./img/icons/ru.svg')}}" type="image/webp">
                  <img src="{{asset('./img/icons/ru.svg')}}" alt=""></picture>
              </a></li>
            <li class="nav__li nav__li_lg"><a href="{{str_replace(url('/') . '/' .  app()->getLocale(), url('/') . '/' .'ro', request()->fullUrl())}}" class="nav__link">
                <picture>
                  <source srcset="{{asset('./img/icons/ro.svg')}}" type="image/webp">
                  <img src="{{asset('./img/icons/ro.svg')}}" alt=""></picture>
              </a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>

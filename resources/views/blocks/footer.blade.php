<!-- Footer -->
<footer class="footer">
  <div class="footer__content _container">
    <div class="footer__row">
      <div class="footer__column">
        <div class="footer__inner">
          <div class="footer__box">
            <div class="footer__text text-block">
              @foreach($footer_texts as $text)
                @if($text->identifier == "footer_text")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <a href="#" class="footer__logo">
              <img src="{{asset(Voyager::image($footer_logo->value))}}" alt="">
            </a>
            <div class="footer__contacts contacts">
              <ul class="contacts__list contacts__list_flexdirection">
                @foreach($footer_links as $link)
                  <li>{{--<i class="{{$link->icon}}"></i>--}}<a href="{{$link->link}}" class="contacts__item {{$link->icon}}">{{$link->text}}</a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="footer__box footer-products">
            <div class="footer__title">
              @foreach($footer_texts as $text)
                @if($text->identifier == "footer_title")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            @foreach($products as $product)
              @if($product->popular_footer == 1)
                <div class="footer-products__row">
                  <div class="footer-products__img _ibg">
                    <img src="{{asset(Voyager::image($product->popular_picture))}}" alt="">
                  </div>
                  @inject('provider', 'App\Http\Controllers\ProductsPageController')
                  <a href="/{{app()->getLocale()}}/products/{{$provider->get_category($product->category_id)->category_slug}}/{{$product->product_slug}}" class="footer-products__text">
                    {{$product->getTranslatedAttribute('title')}}
                  </a>
                </div>
              @endif
            @endforeach
          </div>
        </div>
      </div>
      <div class="footer__column footer-column">
        <div class="footer-column__column">
          <div class="footer__title">
            @foreach($footer_texts as $text)
              @if($text->identifier == "footer_title_menu")
                {{$text->getTranslatedAttribute('text')}}
              @endif
            @endforeach
          </div>
          <ul class="footer-column__list">
            @foreach($menu_constructor as $item)
              <li class="footer-column__li"><a href="/{{app()->getLocale()}}/{{$item->menu_slug}}"
                                               class="footer-column__link">{{$item->getTranslatedAttribute('title')}}</a>
              </li>
            @endforeach
          </ul>
        </div>
        <div class="footer-column__column">
          <div class="footer__title">
            @foreach($footer_texts as $text)
              @if($text->identifier == "footer_useful_links")
                {{$text->getTranslatedAttribute('text')}}
              @endif
            @endforeach
          </div>
          <ul class="footer-column__list">
            @foreach($menu_constructor as $item)
              @if($item->useful_link == 1)
                <li class="footer-column__li"><a href="/{{app()->getLocale()}}/{{$item->menu_slug}}"
                                                 class="footer-column__link">{{$item->getTranslatedAttribute('title')}}</a>
                </li>
              @endif
            @endforeach
          </ul>
        </div>
        <div class="footer-column__column">
          <div class="footer__title">
            @foreach($footer_texts as $text)
              @if($text->identifier == "footer_social_media_title")
                {{$text->getTranslatedAttribute('text')}}
              @endif
            @endforeach
          </div>
          <ul class="footer-column__list footer-column__list_flexdirection">
            <li class="footer-column__li"><a href="#" class="footer-column__link">
                <picture>
                  <source srcset="{{asset('./img/icons/facebook-fill.svg')}}" type="image/webp">
                  <img src="{{asset('./img/icons/facebook-fill.svg')}}" alt=""></picture>
              </a></li>
            <li class="footer-column__li"><a href="#" class="footer-column__link">
                <picture>
                  <source srcset="{{asset('./img/icons/instagram-with-circle.svg')}}" type="image/webp">
                  <img src="{{asset('./img/icons/instagram-with-circle.svg')}}" alt=""></picture>
              </a></li>
            <li class="footer-column__li"><a href="#" class="footer-column__link">
                <picture>
                  <source srcset="{{asset('./img/icons//viber.svg')}}" type="image/webp">
                  <img src="{{asset('./img/icons//viber.svg')}}" alt=""></picture>
              </a></li>
            <li class="footer-column__li"><a href="#" class="footer-column__link">
                <picture>
                  <source srcset="{{asset('./img/icons/whatsapp.svg')}}" type="image/webp">
                  <img src="{{asset('./img/icons/whatsapp.svg')}}" alt=""></picture>
              </a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__copy">
    <div class="footer__autor _container">
      2021 CREATED BY <a href="https://dis.agency" class="logo"></a>
    </div>
  </div>
</footer>

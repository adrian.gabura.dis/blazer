@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}

    {{-- Advantages --}}
    @include('main.advantage'){{-- Implemented --}}

    {{-- About products --}}
    @include('main.about_products'){{-- Implemented --}}

    {{-- Popular Products --}}
    @include('main.products'){{-- Implemented --}}

    {{-- Portofolio --}}
    @include('main.portofolio'){{-- Implemented --}}

    {{-- Reviews --}}
    @include('main.reviews'){{-- Implemented --}}

    {{-- Contact us --}}
    @include('main.contact_us')
  </main>
@endsection

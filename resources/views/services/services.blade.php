@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}
    <div class="services _gallery">
      @foreach($services_items as $services_item)
        @if($services_item->side == "option1")
          <div class="services__row ">
            <div class="services__column">
              <div class="services__content services-content services-content_left">
                <div class="services-content__row">
                  <div class="services-content__img _ibg">
                    <img src="{{asset(Voyager::image($services_item->services_content_image))}}" alt="">
                  </div>
                  <div class="services-content__content">
                    <div
                      class="services-content__text-top">{{$services_item->getTranslatedAttribute('services_content_text_top')}}</div>
                    <div
                      class="services-content__title title-block">{{$services_item->getTranslatedAttribute('services_content_title')}}</div>
                    <span
                      class="services-content__text text-block">{{$services_item->getTranslatedAttribute('services_content_text')}}</span>
                  </div>
                </div>
              </div>
            </div>
            @if($services_item->services_youtube_link == false)
              <div class="services__column ">
                <a href="#" class="services__img _ibg">
                  <img src="{{asset(Voyager::image($services_item->services_main_image))}}" alt="">
                </a>
              </div>
            @elseif($services_item->services_youtube_link == true)
              <div class="services__column ">
                <a href="https://www.youtube.com/watch?v=TgCqbq8p4eA" class="services__img _ibg">
                  <img src="{{asset(Voyager::image($services_item->services_main_image))}}" alt="">
                </a>
              </div>
            @endif
          </div>
        @endif
        @if($services_item->side == "option2")
          <div class="services__row services__row_revers">
            <div class="services__column">
              <div class="services__content services-content services-content_right">
                <div class="services-content__row services-content__row_revers">
                  <div class="services-content__img services-content__img_mleft _ibg">
                    <img src="{{asset(Voyager::image($services_item->services_content_image))}}" alt="">
                  </div>
                  <div class="services-content__content">
                    <div
                      class="services-content__text-top">{{$services_item->getTranslatedAttribute('services_content_text_top')}}</div>
                    <div
                      class="services-content__title title-block">{{$services_item->getTranslatedAttribute('services_content_title')}}</div>
                    <span
                      class="services-content__text text-block">{{$services_item->getTranslatedAttribute('services_content_text')}}</span>
                  </div>
                </div>
              </div>
            </div>
            @if($services_item->services_youtube_link == false)
              <div class="services__column">
                <div class="services__img _ibg services__img_nobefore">
                <img src="{{asset(Voyager::image($services_item->services_main_image))}}" alt="">
                </div>
              </div>
            @elseif($services_item->services_youtube_link == true)
              <div class="services__column">
                <a href="https://www.youtube.com/watch?v=TgCqbq8p4eA" class="services__img _ibg">
                  <img src="{{asset(Voyager::image($services_item->services_main_image))}}" alt="">
                </a>
              </div>
            @endif
          </div>
        @endif
      @endforeach
    </div>
  </main>
@endsection

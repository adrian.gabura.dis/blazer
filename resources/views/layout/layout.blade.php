<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">
@include('blocks.head')
<body>
<div class="wrapper">
  @include('blocks.header')
  @yield('content')
  @include('blocks.footer')
</div>
@include('cookieConsent::index')

<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  window.csrf = $('meta[name="csrf-token"]').attr('content')
</script>
<script src="{{asset('js/vendors.min.js')}}"></script>
<script src="{{asset('js/app.min.js')}}"></script>
<script src="{{asset('js/send_message.js')}}"></script>

<!-- Sweet alert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- AJAX Setup -->

</body>
</html>

@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}

    <div class="product__content _container">
      <div class="product__row">
        @include('products.product_slider')
        <div class="product__column product-info">
          <nav class="product__breadcrumb  breadcrumb">
            <ul class="breadcrumb__list">
              <li><a href="/{{app()->getLocale()}}" class="breadcrumb__link breadcrumb__link">{{$main_page_title}}</a></li>
              <li><a href="/{{app()->getLocale()}}/products/{{$category_slug}}" class="breadcrumb__link breadcrumb__link">{{$category->getTranslatedAttribute('title')}}</a></li>
              <li><a class="breadcrumb__link breadcrumb__link_active">{{$product->getTranslatedAttribute('title')}}</a></li>
            </ul>
          </nav>
          <div class="product-info__title title-block">{{$product->getTranslatedAttribute('title')}}</div>
          <span class="product-info__price">{{$product->price}} $</span>
          <span class="product-info__descr text-block">{{$product->getTranslatedAttribute('desc')}}</span>
        </div>
      </div>
    </div>
  </main>
@endsection

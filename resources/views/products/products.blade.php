@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}
    <div class="category">
      <div class="category__content _container">
        <nav class="product__breadcrumb  breadcrumb">
          <ul class="breadcrumb__list">
            <li><a href="/{{app()->getLocale()}}" class="breadcrumb__link breadcrumb__link">{{$menu_item_title_main}}</a></li>
            <li><a class="breadcrumb__link breadcrumb__link_active">{{$menu_item_title_products}}</a></li>
          </ul>
        </nav>
        <div class="category__row">
          @foreach($categories as $category)
            <div class="category__column"><a href="/{{app()->getLocale()}}/products/{{$category->category_slug}}" class="category__items ">
                <div class="category__img _ibg">
                  <img src="{{asset(Voyager::image($category->image))}}" alt="">
                </div>
                <div class="category__info">
                  <div class="category__title">{{$category->getTranslatedAttribute('title')}}</div>
                  @inject('provider', 'App\Http\Controllers\ProductsPageController')
                  <div class="category__canty">{{$provider::category_products_count(1)}} products</div>
                </div>
              </a></div>
          @endforeach
        </div>
      </div>
    </div>
  </main>
@endsection

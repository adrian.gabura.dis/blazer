@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}
    <div class="catalog">
      <div class="catalog__content _container ">
        <div class="catalog__row">
          @foreach($category_products as $category_product)
            <a href="/{{app()->getLocale()}}/products/{{$category->category_slug}}/{{$category_product->product_slug}}" class="catalog__column">
              @if($category_product->new == "1")
                <span class="products__labels">New</span>
              @endif
              <div class="catalog__img _ibg">
                <img src="{{asset(Voyager::image($category_product->popular_picture))}}" alt="">
              </div>
              <span class="catalog__name">{{$category_product->getTranslatedAttribute('title')}}</span>
              <span class="catalog__subname">{{$category_product->getTranslatedAttribute('subtitle')}}</span>
              <span class="catalog__price">{{$category_product->price}}$</span>
            </a>
          @endforeach
        </div>
      </div>
  </main>
@endsection

<div class="product__column product-slide">
  <div class="product-slide__nav nav-slide ">
    <div class="nav-slide__items gallery-thumbs _swiper">
      @foreach($product_images as $image)
        <div class="nav-slide__item">
          <img src="{{asset(Voyager::image($image->image))}}" alt="">
        </div>
      @endforeach
    </div>
    <div class="nav-slide__control">
      <div class="nav-slide__dow"></div>
      <div class="nav-slide__up"></div>
    </div>
  </div>
  <div class="product-slide__left">
    <div class="product-slide__items gallery-top _swiper">
      @foreach($product_images as $image)
        <div class="product-slide__item">
          <img src="{{asset(Voyager::image($image->image))}}" alt="">
        </div>
      @endforeach
    </div>
  </div>
</div>

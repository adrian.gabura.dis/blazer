<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="admin login">
    <title>@yield('title', 'Admin - '.Voyager::setting("admin.title"))</title>
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
    @if (__('voyager::generic.is_rtl') == 'true')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif
    <style>
        body {
            background-image:url('{{ Voyager::image( Voyager::setting("admin.bg_image"), voyager_asset("images/bg.jpg") ) }}');
            background-color: {{ Voyager::setting("admin.bg_color", "#FFFFFF" ) }};
        }
        body.login .login-sidebar {
            border-top:5px solid {{ config('voyager.primary_color','#22A7F0') }};
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid {{ config('voyager.primary_color','#22A7F0') }};
            }
        }
        body.login .form-group-default.focused{
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .login-button, .bar:before, .bar:after{
            background:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .remember-me-text{
            padding:0 5px;
        }
        body.login .logo {
            width: 100%;
            max-width: 135px;
        }
        body.login .login-container {
            top: 35%;
        }
        body.login .login-container .logo {
            max-width: 200px;
        }

                /* DIS logo */
        .footer__logo__dis {
            display: inline-block;
            position: relative;
            z-index: 100;
            width: 250px;
            height: 165px;
            margin-left: 5px;
            display: flex;
            align-items: center;
        }
        .footer__logo__dis__left{

            margin-left: 0;
            width: 100px;
            height: 65px;
        }

        .footer__logo__dis::before {
            content: '';
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            -webkit-filter: blur(5px);
            filter: blur(5px);
            -webkit-animation: blur 6s infinite;
            animation: blur 6s infinite;
            background-image: url('/images/dis/logo-w.webp');
        }
        /* ._webp .logo::after {
            background-image: url(../img/dis-b.webp);
        } */
        .footer__logo__dis::after {
            content: '';
            display: block;
            position: absolute;
            z-index: 2;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            background-image: url('/images/dis/dis-b.webp');

        }

        .footer__logo__dis__text {
            margin-left: 5px;
        }

        @keyframes blur {
            0%   {
                filter: blur(3px);
            }
            50% {
                filter: blur(10px);
            }
            100% {
                filter: blur(3px);
            }
        }
        .logo-title-container{
            display: flex;
            align-items: center;
        }
        /* end DIS logo */

    </style>

    @yield('pre_css')
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
</head>
<body class="login">
<div class="container-fluid">
    <div class="row">
        <div class="faded-bg animated"></div>
        <div class="hidden-xs col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2">
                    <div class="logo-title-container">
                        <div class="footer__logo__dis footer__logo__dis__left">
                        </div>
                    @php /*    
                    <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" 
                            src="{{ asset('images/dis/dis_logo.png') }}" alt="Logo Icon">
                          */  @endphp
                        <div class="copy animated fadeIn">
                            <h1>{{ Voyager::setting('admin.title', 'Voyager') }}</h1>
                            <p>{{ Voyager::setting('admin.description', __('voyager::login.welcome')) }}</p>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

            @yield('content')

        </div> <!-- .login-sidebar -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
@yield('post_js')
</body>
</html>

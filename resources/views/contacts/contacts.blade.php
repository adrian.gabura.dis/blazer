@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}
    <div class="contacts-map">
      @foreach($contact_us_texts as $text)
        @if($text->identifier == "google_map_link")
          <iframe class="contacts-us__iframe contacts-us__iframe_contacts" src="{{$text->text}}" allowfullscreen="" loading="lazy"></iframe>
        @endif
      @endforeach
    </div>
    <div class="questions">
      <div class="questions__content _container">
        <div class="questions__row">
          <div class="questions__column">
            <div class="questions__descr text-label">
              @foreach($contact_us_texts as $text)
                @if($text->identifier == "questions_desc_1")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <div class="questions__title title-block">
              @foreach($contact_us_texts as $text)
                @if($text->identifier == "questions_title_1")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <div class="questions__spoilers spollers-block  _spollers _one">
              <div class="">
                @foreach($frequent_questions as $frequent_question)
                  <div class="spollers-block__item">
                    <div class="spollers-block__title _spoller">{{$frequent_question->getTranslatedAttribute('title')}}</div>
                    <div class="spollers-block__body">
                      {{$frequent_question->getTranslatedAttribute('text')}}
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
          <div class="questions__column">
            <div class="questions__descr text-label">
              @foreach($contact_us_texts as $text)
                @if($text->identifier == "questions_desc_2")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <div class="questions__title title-block">
              @foreach($contact_us_texts as $text)
                @if($text->identifier == "questions_title_2")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <form onsubmit="send_message(event)" action="javascript:void(0)" method="POST" class="contacts-us__form form _scr-item">
              @csrf
              <div class="form__inner">
                <div class="form__line">
                  <label for="name" class="form__label">
                    @foreach($contact_us_texts as $text)
                      @if($text->identifier == "questions_your_name")
                        {{$text->getTranslatedAttribute('text')}}
                      @endif
                    @endforeach
                  </label>
                  <input required id="name" type="text" name="form[]" class="form__input _date">
                </div>
                <div class="form__line">
                  <label for="mail" class="form__label">
                    @foreach($contact_us_texts as $text)
                      @if($text->identifier == "questions_your_mail")
                        {{$text->getTranslatedAttribute('text')}}
                      @endif
                    @endforeach
                  </label>
                  <input required id="mail" type="text" name="form[]" class="form__input _date">
                </div>
                <div class="form__line">
                  <label for="phone" class="form__label">
                    @foreach($contact_us_texts as $text)
                      @if($text->identifier == "questions_your_number")
                        {{$text->getTranslatedAttribute('text')}}
                      @endif
                    @endforeach
                  </label>
                  <input required id="phone" type="text" name="form[]" class="form__input _date">
                </div>
                <div class="form__line">
                  <label for="company" class="form__label">
                    @foreach($contact_us_texts as $text)
                      @if($text->identifier == "questions_your_company")
                        {{$text->getTranslatedAttribute('text')}}
                      @endif
                    @endforeach
                  </label>
                  <input required id="company" autocomplete="off" type="text" name="form[]" class="form__input _date">
                </div>
              </div>
              <div class="form__line-textarea">
                <label for="message" class="form__label">
                  @foreach($contact_us_texts as $text)
                    @if($text->identifier == "questions_your_message")
                      {{$text->getTranslatedAttribute('text')}}
                    @endif
                  @endforeach
                </label>
                <textarea required name="form[]" id="message" class="form__textare"></textarea>
              </div>
              <button type="submit" class="form__btn btn-block">
                @foreach($contact_us_texts as $text)
                  @if($text->identifier == "questions_send_message")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </main>
@endsection

@extends('layout.layout')

@section('content')
  <main class="page">
    {{-- Main sliders --}}
    @include('blocks.sliders'){{-- Implemented --}}
    <div class="abaut">
      <div class="abaut__content _container">
        <div class="abaut__row">
          <div class="abaut__column">
            <div class="abaut__img _ibg">
              <img src="{{asset(Voyager::image($about_image->image))}}" alt="">
            </div>
          </div>
          <div class="abaut__column">
            <div class="abaut__info">
              <div class="abaut__descr text-label">
                @foreach($texts as $text)
                  @if($text->identifier == "about_description")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </div>
              <div class="abaut__title title-block">
                @foreach($texts as $text)
                  @if($text->identifier == "about_title")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </div>
              <div class="abaut__subtitle">
                @foreach($texts as $text)
                  @if($text->identifier == "about_subtitle")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </div>
              <span class="abaut__text text-block">
                @foreach($texts as $text)
                  @if($text->identifier == "about_text1")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </span>
              <span class="abaut__text text-block">
                 @foreach($texts as $text)
                  @if($text->identifier == "about_text2")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="experience _ibg">
      <img src="{{asset(Voyager::image($about_image_experience->image))}}" alt="">
      <div class="experience__content _container">
        <div class="experience__row">
          <div class="experience__column">
            <div class="experience__title title-block title-block_w">
              @foreach($texts as $text)
                @if($text->identifier == "experience_title")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <div class="experience__subtitle">
              @foreach($texts as $text)
                @if($text->identifier == "experience_subtitle")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
          </div>
          <div class="experience__column experience-row">
            @foreach($experiences as $experience)
              <div class="experience-row__items"><span class="experience-row__nr">{{$experience->getTranslatedAttribute('title')}}</span> <span
                  class="experience-row__text">{{$experience->getTranslatedAttribute('text')}}</span></div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </main>
@endsection

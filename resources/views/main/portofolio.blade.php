<div class="portofoliu">
  <div class="portofoliu__content _container">
    <div class="portofoliu__title  title-block">
      @foreach($texts as $text)
        @if($text->identifier == "portofolio_title")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <span class="portofoliu__text advantage__text text-block">
      @foreach($texts as $text)
        @if($text->identifier == "portofolio_text")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </span>
    <div class="portofoliu__sliders">
      <div class="portofoliu__slider _swiper">
        @foreach($portofolios as $portofolio)
          <div class="portofoliu__slide">
            <div class="portofoliu__img _ibg">
              <img src="{{asset(Voyager::image($portofolio->image))}}" alt="">
            </div>
          </div>
        @endforeach
      </div>
      <div class="portofoliu__left products__left"></div>
      <div class="portofoliu__right products__right"></div>
    </div>
  </div>
</div>

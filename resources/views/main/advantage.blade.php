<div class="advantage">
  <div class="advantage__wrapper _container">
    <div class="advantage__label text-label">
      @foreach($texts as $text)
        @if($text->identifier == "advantages_label")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <h1 class="advantage__title title-block">
      @foreach($texts as $text)
        @if($text->identifier == "advantages_title")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </h1>
    <span class="advantage__text text-block">
			@foreach($texts as $text)
        @if($text->identifier == "advantages_text")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </span>
    <div class="advantage__row">
      <div class="advantage__column">
        @foreach($advantages as $advantage)
          @if($advantage->side == "option1")
            <div class="advantage__items">
              <div class="advantage__content advantage__content_right">
                <span
                  class="advantage__name title-block title-block_ftsz">{{$advantage->getTranslatedAttribute('title')}}</span>
                <span class="advantage__descr advantage__descr_right text-block">
											{{$advantage->getTranslatedAttribute('desc')}}
            </span>
              </div>
              <div class="advantage__icon">
                {{--<i class="{{$advantage->icon}}"></i>--}}
                <img src="{{asset(Voyager::image($advantage->icon_image))}}" alt="">
              </div>
            </div>
          @endif
        @endforeach
      </div>
      <div class="advantage__column advantage__column_images">
        <div class="advantage__images">
          <picture>
            <source srcset="./img/benefits/02.webp" type="image/webp">
            <img src="./img/benefits/02.png" alt="" class="advantage__img1"></picture>
          <picture>
            <source srcset="./img/02.webp" type="image/webp">
            <img src="./img/02.png" alt="" class="advantage__img2"></picture>
        </div>
      </div>
      <div class="advantage__column">
        @foreach($advantages as $advantage)
          @if($advantage->side == "option2")
            <div class="advantage__items advantage__items_revers">
              <div class="advantage__content">
                <span
                  class="advantage__name title-block title-block_ftsz">{{$advantage->getTranslatedAttribute('title')}}</span>
                <span class="advantage__descr text-block">
											{{$advantage->getTranslatedAttribute('desc')}}
                </span>
              </div>
              <div class="advantage__icon">
                {{--<i class="{{$advantage->icon}}"></i>--}}
                <img src="{{asset(Voyager::image($advantage->icon_image))}}" alt="">
              </div>
            </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
</div>

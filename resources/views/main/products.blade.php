<div class="products">
  <div class="products__content _container">
    <div class="products__label  text-label">
      @foreach($texts as $text)
        @if($text->identifier == "products_label")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <div class="products__title  title-block">
      @foreach($texts as $text)
        @if($text->identifier == "products_title")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <span class="products__text advantage__text text-block">
      @foreach($texts as $text)
        @if($text->identifier == "products_text")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </span>
    <div class="products__sliders">
      <div class="products__slider _swiper">
        @foreach($products as $product)
          <div class="products__slide">
            @if($product->new == 1)
              <span class="products__labels">New</span>
            @endif
            <div class="products__img _ibg">
              <img src="{{asset(Voyager::image($product->popular_picture))}}" alt="">
            </div>
            <div>{{$product->category->category_slug}}</div>
            <a href="/{{app()->getLocale()}}/products/{{$product->category->category_slug}}/{{$product->product_slug}}"
               class="products__name">{{$product->getTranslatedAttribute('title')}}</a>
            <span class="products__category">{{$product->category->title}}</span>
          </div>
        @endforeach
      </div>

      {{-- Slider arrows --}}
      <div class="products__left"></div>
      <div class="products__right"></div>
    </div>
  </div>
</div>

<div class="abaut-products">
  <div class="abaut-products__row _scr-item">
    <div class="abaut-products__column">
      <div class="abaut-products__bg _ibg">
        @foreach($images as $image)
          @if($image->identifier == "about_products_background")
            <img src="{{asset(Voyager::image($image->image))}}" alt="">
          @endif
        @endforeach
      </div>
      <div class="abaut-products__img abaut-products__img_right">
        @foreach($images as $image)
          @if($image->identifier == "about_products_image")
            <img src="{{asset(Voyager::image($image->image))}}" alt="">
          @endif
        @endforeach
      </div>
    </div>
    <div class="abaut-products__column">
      <div class="abaut-products__content">
        <div class="abaut-products__descr text-label">
          @foreach($texts as $text)
            @if($text->identifier == "about_products_desc_1")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </div>
        <span class="abaut-products__title title-block">
          @foreach($texts as $text)
            @if($text->identifier == "about_products_title_1")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </span>
        <span class="abaut-products__text text-block">
          @foreach($texts as $text)
            @if($text->identifier == "about_products_text_1")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </span>
        <div class="abaut-products__links">
          <a href="#" class="abaut-products__link btn-block">
            @foreach($texts as $text)
              @if($text->identifier == "about_products_1_link_text_1")
                {{$text->getTranslatedAttribute('text')}}
              @endif
            @endforeach
          </a>
          <a href="#" class="abaut-products__link btn-block btn-block_black">
            @foreach($texts as $text)
              @if($text->identifier == "about_products_1_link_text_2")
                {{$text->getTranslatedAttribute('text')}}
              @endif
            @endforeach
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="abaut-products__row abaut-products__row_revers _scr-item">
    <div class="abaut-products__column">
      <div class="abaut-products__bg _ibg">
        @foreach($images as $image)
          @if($image->identifier == "about_products_background_2")
            <img src="{{asset(Voyager::image($image->image))}}" alt="">
          @endif
        @endforeach
      </div>
      <div class="abaut-products__img abaut-products__img_left">
        @foreach($images as $image)
          @if($image->identifier == "about_products_image_2")
            <img src="{{asset(Voyager::image($image->image))}}" alt="">
          @endif
        @endforeach
      </div>
    </div>
    <div class="abaut-products__column">
      <div class="abaut-products__content">
        <div class="abaut-products__descr text-label">
          @foreach($texts as $text)
            @if($text->identifier == "about_products_2_desc")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </div>
        <span class="abaut-products__title title-block">
          @foreach($texts as $text)
            @if($text->identifier == "about_products_2_title")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </span>
        <span class="abaut-products__text text-block">
          @foreach($texts as $text)
            @if($text->identifier == "about_products_2_text")
              {{$text->getTranslatedAttribute('text')}}
            @endif
          @endforeach
        </span>
        <div class="abaut-products__numbers numbers-row">
          <div class="numbers-row__column">
            <div class="numbers-row__number">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_1")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <div class="numbers-row__description text-block">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_desc_1")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
          </div>
          <div class="numbers-row__column">
            <div class="numbers-row__number">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_2")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
            <div class="numbers-row__description text-block">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_desc_2")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
          </div>
          <div class="numbers-row__column">
            <div class="numbers-row__number">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_3")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach</div>
            <div class="numbers-row__description text-block">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_desc_3")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
          </div>
          <div class="numbers-row__column">
            <div class="numbers-row__number">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_4")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach</div>
            <div class="numbers-row__description text-block">
              @foreach($texts as $text)
                @if($text->identifier == "about_products_numbers_desc_4")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

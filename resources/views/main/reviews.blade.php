<div class="reviews">
  <div class="reviews__content _container">
    <div class="reviews__title  title-block">
      @foreach($texts as $text)
        @if($text->identifier == "reviews_title")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <div class="reviews__sliders">
      <div class="reviews__slider _swiper">
        @foreach($reviews as $review)
          <div class="reviews__slide">
            <div class="reviews__row">
              <div class="reviews__img">
                <img src="{{asset(Voyager::image( $review->image))}}" alt="">
              </div>
              <div class="reviews__message">
                <div class="reviews__name">{{$review->getTranslatedAttribute('name')}}</div>
                <span class="reviews__text">
							  {{$review->getTranslatedAttribute('text')}}
              </span>
                <div class="reviews__star star">
												<span class="star__item">
													<picture><source srcset="./img/icons/star-fill.svg" type="image/webp"><img
                              src="./img/icons/star-fill.svg" alt=""></picture>
												</span>
                  <span class="star__item">
													<picture><source srcset="./img/icons/star-fill.svg" type="image/webp"><img
                              src="./img/icons/star-fill.svg" alt=""></picture>
												</span>
                  <span class="star__item">
													<picture><source srcset="./img/icons/star-fill.svg" type="image/webp"><img
                              src="./img/icons/star-fill.svg" alt=""></picture>
												</span>
                  <span class="star__item">
													<picture><source srcset="./img/icons/star-fill.svg" type="image/webp"><img
                              src="./img/icons/star-fill.svg" alt=""></picture>
												</span>
                  <span class="star__item">
													<picture><source srcset="./img/icons/star-fill.svg" type="image/webp"><img
                              src="./img/icons/star-fill.svg" alt=""></picture>
												</span>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <div class="reviews__control">
        <div class="reviews__left">
          <svg class="svg-inline--fa fa-angle-left fa-w-8 arrow__icon" aria-hidden="true" focusable="false"
               data-prefix="fas" data-icon="angle-left" role="img" xmlns="http://www.w3.org/2000/svg"
               viewBox="0 0 256 512" data-fa-i2svg="">
            <path fill="currentColor"
                  d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z"></path>
          </svg>
        </div>
        <div class="reviews__right">
          <svg class="svg-inline--fa fa-angle-right fa-w-8 arrow__icon" aria-hidden="true" focusable="false"
               data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg"
               viewBox="0 0 256 512" data-fa-i2svg="">
            <path fill="currentColor"
                  d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path>
          </svg>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="contacts-us">
  <div class="contacts-us__content _container">
    <div class="contacts-us__descr text-label">
      @foreach($texts as $text)
        @if($text->identifier == "contact_us_desc")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <div class="contacts-us__title title-block">
      @foreach($texts as $text)
        @if($text->identifier == "contact_us_title")
          {{$text->getTranslatedAttribute('text')}}
        @endif
      @endforeach
    </div>
    <div class="contacts-us__row">
      <div class="contacts-us__column">
        <form onsubmit="send_message(event)" action="javascript:void(0)" method="POST" class="contacts-us__form form _scr-item">
          @csrf
          <div class="form__inner">
            <div class="form__line">
              <label for="name" class="form__label">
                @foreach($texts as $text)
                  @if($text->identifier == "contact_us_name")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </label>
              <input required id="name" type="text" name="form[]" class="form__input _date">
            </div>
            <div class="form__line">
              <label for="mail" class="form__label">
                @foreach($texts as $text)
                  @if($text->identifier == "contact_us_email")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </label>
              <input required id="mail" type="text" name="form[]" class="form__input _date">
            </div>
            <div class="form__line">
              <label for="phone" class="form__label">
                @foreach($texts as $text)
                  @if($text->identifier == "contact_us_phone")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </label>
              <input required id="phone" type="text" name="form[]" class="form__input _date">
            </div>
            <div class="form__line">
              <label for="company" class="form__label">
                @foreach($texts as $text)
                  @if($text->identifier == "contact_us_company")
                    {{$text->getTranslatedAttribute('text')}}
                  @endif
                @endforeach
              </label>
              <input required id="company" autocomplete="off" type="text" name="form[]" " class="form__input _date">
            </div>
          </div>
          <div class="form__line-textarea">
            <label for="message" class="form__label">
              @foreach($texts as $text)
                @if($text->identifier == "contact_us_message")
                  {{$text->getTranslatedAttribute('text')}}
                @endif
              @endforeach
            </label>
            <textarea name="form[]" id="message" class="form__textare"></textarea>
          </div>
          <button type="submit" class="form__btn btn-block">
            @foreach($texts as $text)
              @if($text->identifier == "contact_us_send")
                {{$text->getTranslatedAttribute('text')}}
              @endif
            @endforeach
          </button>
        </form>
      </div>
      <div class="contacts-us__column">
        <div class="contacts-us__map">
          @foreach($texts as $text)
            @if($text->identifier == "google_map_link")
              <iframe class="contacts-us__iframe" src="{{$text->text}}" allowfullscreen="" loading="lazy"></iframe>
            @endif
          @endforeach

        </div>
      </div>
    </div>
  </div>
</div>

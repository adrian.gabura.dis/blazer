<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'js_sended' => 'Ваше сообщение отправлено успешно',
    'js_success_msgs' => 'Вскоре мы вам ответим',
    'js_send_error' => 'Ошибка',
    'js_error_msg' => 'Пожалуйста, повторите попытку позже, сервис временно недоступен',
    'js_subs_success' => 'Успех',
    'js_subs_success_message' => 'Подписка оформлена успешно',


    'h_contact' => 'Контактная информация',


    'index_all_services' => 'Все услуги',
    'index_detail' => 'Подробнее',
    'prod_Categories' => 'Категории продуктов',
    'news_Categories' => 'Категории новостей',


    'banner_home' => 'Главная',
    'banner_portfolio' => 'Портфолио',
    'banner_news' => 'Новости',
    'banner_services' => 'Услуги',
    'banner_products' => 'Продукты',
    'banner_vacancies' => 'Вакансии',
    'banner_partners' => 'Партнеры',


    'contact_send_message' => 'Отправить сообщение',
    'contact_message' => 'Сообщение',
    'contact_subject' => 'Тема письма',
    'contact_phone' => 'Номер телефона',
    'contact_email' => 'Е-майл',
    'contact_name' => 'Имя',


    'search_result' => 'Результаты поиска по слову ',
    'search_results' => 'результатов',
    'search_v' => 'Вакансии',
    'search_n' => 'Новости',
    'search_p' => 'Товары',
    'search_s' => 'Услуги',


    'footer_email' => 'Эл. почта',


    'search_product' => 'Поиск товара',
    'search_search' => 'Искать...',

];

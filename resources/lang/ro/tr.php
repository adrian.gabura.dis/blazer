<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'js_sended' => 'Mesajul dvs a fost expediat cu succes',
    'js_success_msgs' => 'Mulțumim! in curînd va vom răspunde',
    'js_send_error' => 'Eroare',
    'js_error_msg' => 'Va rugam sa mai încercați încă odata mai târziu',
    'js_subs_success' => 'Succes',
    'js_subs_success_message' => 'Vă mulțumim pentru înscriere',


    'h_contact' => 'Contactele noastre',


    'index_all_services' => 'Vezi taoate serviciile',
    'index_detail' => 'Detalii',
    'prod_Categories' => 'Categoriile produselor',
    'news_Categories' => 'Categoriile noutăților',


    'banner_home' => 'Acasă',
    'banner_portfolio' => 'Portofoliu',
    'banner_news' => 'Noutăți',
    'banner_services' => 'Servicii',
    'banner_products' => 'Produse',
    'banner_vacancies' => 'Psoturi vacante',
    'banner_partners' => 'Partenerii',


    'contact_send_message' => 'Trimite mesajul',
    'contact_message' => 'Mesajul',
    'contact_subject' => 'Tema',
    'contact_phone' => 'Nr. de telefon',
    'contact_email' => 'E-mail',
    'contact_name' => 'Numele',


    'search_result' => 'Căutarea după cuvântul',
    'search_results' => 'rezultate',
    'search_v' => 'Posturi vacante',
    'search_n' => 'Noutăți',
    'search_p' => 'Produse',
    'search_s' => 'Servicii',


    'footer_email' => 'E-mail',


    'search_product' => 'Căutarea produsului',
    'search_search' => 'Caută...',
];
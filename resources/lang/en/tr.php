<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'js_sended' => 'Your message has been sent successfully',
    'js_success_msgs' => 'Thank you! we will answer you soon',
    'js_send_error' => 'Error',
    'js_error_msg' => 'Please try again later',
    'js_subs_success' => 'Success',
    'js_subs_success_message' => 'Thanks for signing up',


    'h_contact' => 'Our contacts',


    'index_all_services' => 'See all services',
    'index_detail' => 'Details',
    'prod_Categories' => 'Product categories',
    'news_Categories' => 'News categories',


    'banner_home' => 'Home',
    'banner_portfolio' => 'Portfolio',
    'banner_news' => 'News',
    'banner_services' => 'Services',
    'banner_products' => 'Products',
    'banner_vacancies' => 'Vacancies',
    'banner_partners' => 'Partners',


    'contact_send_message' => 'Send',
    'contact_message' => 'Message',
    'contact_subject' => 'Theme',
    'contact_phone' => 'Nr. by phone',
    'contact_email' => 'E-mail',
    'contact_name' => 'Name',


    'search_result' => 'Searching for the word',
    'search_results' => 'Result',
    'search_v' => 'Vacancies',
    'search_n' => 'News',
    'search_p' => 'Products',
    'search_s' => 'Services',


    'footer_email' => 'E-mail',


    'search_product' => 'Search product',
    'search_search' => 'search...',
];
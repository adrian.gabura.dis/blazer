<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
*/
Route::group(['prefix' => 'admin'], function () {
  Voyager::routes();
  Route::get('login', [\App\Http\Controllers\Auth\LoginController::class, 'voyagerLogin'])->name('voyager.login');
});


Route::group([
  'prefix' => '{locale}',
  'as' => 'locale.',
  'where' => ['locale' => '[a-zA-Z]{2}'],
  'where' => ['locale' => '(ru|ro)'],
  'middleware' => 'locale'
], function ($locale) {

  Route::get('/', ['uses' => 'App\Http\Controllers\MainPageController@mainPage']);

  Route::get('/about', ['uses' => 'App\Http\Controllers\AboutPageController@aboutPage']);

  Route::get('/news', ['uses' => 'App\Http\Controllers\NewsPageController@newsPage']);
  Route::get('/news/{slug}', ['uses' => 'App\Http\Controllers\NewsPageController@newsSinglePage']);

  //Route::get('/products/{category_slug}/{product_slug}', ['uses' => 'App\Http\Controllers\ProductsPageController@productDetails']);

  Route::get('/services', ['uses' => 'App\Http\Controllers\ServicesPageController@servicesPage']);

  Route::get('/products', ['uses' => 'App\Http\Controllers\ProductsPageController@productsPage']);
  Route::get('/products/{category_slug}', ['uses' => 'App\Http\Controllers\ProductsPageController@categoryPage']);
  Route::get('/products/{category_slug}/{product_slug}', ['uses' => 'App\Http\Controllers\ProductsPageController@productDetailsPage']);

  //Route::get('/products', ['uses' => 'App\Http\Controllers\ProductsPageController@productsPage']);
  //Route::get('/products/{slug}', ['uses' => 'App\Http\Controllers\ProductsPageController@productsCategoryPage']);
  //Route::get('/products/{slug}/{slug2}', ['uses' => 'App\Http\Controllers\ProductsPageController@productsDetailPage']);

  Route::get('/contacts', ['uses' => 'App\Http\Controllers\ContactsPageController@contactsPage']);

  Route::get('/privacy', ['uses' => 'App\Http\Controllers\ContactsPageController@privacyPage']);

  Route::get('/products-search', ['uses' => 'App\Http\Controllers\SearchPageController@search']);
});

Route::get('/', function () {
  return redirect('/ro');
});

Route::post('/mail', ['uses' => 'App\Http\Controllers\ContactsPageController@sendMessage']);
//Route::post('/subscribe', ['uses' => 'App\Http\Controllers\ContactsPageController@subscribe']);


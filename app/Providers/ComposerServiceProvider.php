<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Composers\HeaderComposer;
use App\Http\Composers\FooterComposer;
use App\Http\Composers\AllPagesComposer;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'blocks.header', HeaderComposer::class
        );
        View::composer(
            'blocks.footer', FooterComposer::class
        );
        View::composer(
            '*', AllPagesComposer::class
        );
    }
}

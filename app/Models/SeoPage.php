<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class SeoPage extends Model
{
    use HasFactory;
    use Translatable;
    protected $translatable = ['title', 'seo_title', 'meta_keywords', 'meta_descriptions'];
}
// use TCG\Voyager\Traits\Translatable;
//     use Translatable;
//     protected $translatable = ['title', 'text', 'seo_title', 'meta_keywords', 'meta_descriptions'];
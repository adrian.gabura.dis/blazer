<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ServicesItem extends Model
{
  use HasFactory;
  use Translatable;
  protected $translatable = ['services_content_text_top', 'services_content_title', 'services_content_text'];
}

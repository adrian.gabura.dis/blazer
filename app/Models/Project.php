<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
}
// use TCG\Voyager\Traits\Translatable;
//     use Translatable;
//     protected $translatable = ['title', 'text', 'seo_title', 'meta_keywords', 'meta_descriptions'];
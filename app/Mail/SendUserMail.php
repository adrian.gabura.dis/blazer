<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use TCG\Voyager\Models\Setting;

class SendUserMail extends Mailable
{
  use Queueable, SerializesModels;

  public $email_to;
  public $name;
  public $mail;
  public $phone;
  public $company;
  public $mail_message;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($email_to, $name, $mail, $phone, $company, $mail_message)
  {
    $this->email_to = $email_to;
    $this->name = $name;
    $this->mail = $mail;
    $this->phone = $phone;
    $this->company = $company;
    $this->mail_message = $mail_message;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->subject('Компания "Blazer" приветствует Вас')->view('blocks.form_send_user_messsage')->with([
      'email_to'=>$this->email_to,
      'name'=>$this->name,
      'mail'=>$this->mail,
      'phone'=>$this->phone,
      'company'=>$this->company,
      'mail_message'=>$this->mail_message
    ]);
    /*return $this->from('no-reply@dis.md')
      ->subject('Новый пользователь оставил сообщение на сайте dis.agency')
      ->view('blocks.form_send_message')
      ->to($this->email_to, '')
      ->with([
        'name' => $this->name,
        'mail' => $this->mail,
        'phone' => $this->phone,
        'company' => $this->company,
        'message' => $this->message,
      ]);*/
  }
}

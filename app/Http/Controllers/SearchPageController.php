<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SeoPage;
use App\Models\Product;
use App\Models\PageVocabulary;

class SearchPageController extends Controller
{
    public function search($lang, Request $request){
        $page       = SeoPage::where('id', '=', '8')->first();
        $banner     = SeoPage::where('id', '=', '8')->first();
        $vocabulary = PageVocabulary::orderBy('id', 'asc')->get();
    	$key = $request->search;

        $products = Product::whereTranslation('title', 'LIKE', '%' . $key . '%', ['ru', 'ro', 'en'])
        					->orderBy('order', 'desc')
        					->paginate(1)
        					->withQueryString();
        return view('products-search', compact('page','banner','vocabulary','products','key'));
    }
}

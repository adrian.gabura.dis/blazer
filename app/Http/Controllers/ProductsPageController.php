<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\FooterLink;
use App\Models\FooterText;
use App\Models\HeaderLink;
use App\Models\HeaderText;
use App\Models\MenuLink;
use App\Models\Product;
use App\Models\TopSlider;
use App\Models\SeoPage;
use App\Models\ProductCategory;
use App\Models\ProductImage;

class ProductsPageController extends Controller
{
  public function productDetailsPage($lang, $category_slug, $product_slug)
  {
    $page = SeoPage::where('id', '=', '1')->with('translations')->firstOrFail();

    $header_links = HeaderLink::get();
    $header_texts = HeaderText::get();
    $footer_links = FooterLink::get();
    $footer_texts = FooterText::get();

    $top_sliders = TopSlider::get();

    $products = Product::where('popular', '=', '1')->get();

    $menu_constructor = MenuLink::get();

    $header_logo = Setting::where('key', '=', 'site.logo')->first();
    $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

    $product = Product::where('product_slug', '=', $product_slug)->first();
    $product_images = ProductImage::where('product_id', '=', $product->id)->get();

    $category = ProductCategory::where('category_slug', '=', $category_slug)->first();
    $main_page_title = MenuLink::where('id', '=', 1)->first()->title;

    return view('products.product_details', compact(
      'page',
    'header_links',
      'header_texts',
      'footer_links',
      'footer_texts',
      'top_sliders',
      'products',
      'menu_constructor',
      'product',
      'category_slug',
      'category',
      'main_page_title',
      'product_images',
      'header_logo',
      'footer_logo'
    ));
  }

  public function categoryPage($lang, $category_slug)
  {
    $page = SeoPage::where('id', '=', '1')->with('translations')->firstOrFail();

    $header_links = HeaderLink::get();
    $header_texts = HeaderText::get();
    $footer_links = FooterLink::get();
    $footer_texts = FooterText::get();

    $top_sliders = TopSlider::get();

    $products = Product::where('popular', '=', '1')->get();

    $menu_constructor = MenuLink::get();

    $header_logo = Setting::where('key', '=', 'site.logo')->first();
    $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

    $category = ProductCategory::where('category_slug', '=', $category_slug)->first();
    $category_id = $category->id;
    $category_products = Product::where('category_id', '=', $category_id)->get();

    return view('products.category', compact(
      'page',
      'header_texts',
      'header_links',
      'footer_texts',
      'footer_links',
      'top_sliders',
      'products',
      'menu_constructor',
      'category_products',
      'category',
      'header_logo',
      'footer_logo'
    ));
  }

  static function category_products_count($id)
  {
    $prods = Product::where('category_id', '=', $id)->get();
    $numb = count($prods);
    return $numb;
  }

  static function get_category($id)
  {
    $category = ProductCategory::where('id', '=', $id)->first();
    return $category;
  }

  public function productsPage()
  {
    $page = SeoPage::where('id', '=', '1')->with('translations')->firstOrFail();

    $header_links = HeaderLink::get();
    $header_texts = HeaderText::get();
    $footer_links = FooterLink::get();
    $footer_texts = FooterText::get();

    $top_sliders = TopSlider::get();

    $products = Product::where('popular', '=', '1')->get();

    $menu_constructor = MenuLink::get();
    $header_logo = Setting::where('key', '=', 'site.logo')->first();
    $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

    $categories = ProductCategory::get();
    $menu_item_title_main = MenuLink::where('id', '=', 1)->first()->getTranslatedAttribute('title');
    $menu_item_title_products = MenuLink::where('menu_slug', '=', "products")->first()->getTranslatedAttribute('title');

    return view('products.products',
      compact(
        'page',
        'header_links',
        'header_texts',
        'footer_links',
        'footer_texts',
        'top_sliders',
        'products',
        'menu_constructor',
        'categories',
        'menu_item_title_main',
        'menu_item_title_products',
        'header_logo',
        'footer_logo'
      ));
  }
}

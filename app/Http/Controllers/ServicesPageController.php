<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\AboutPageText;
use App\Models\FooterLink;
use App\Models\FooterText;
use App\Models\HeaderLink;
use App\Models\HeaderText;
use App\Models\MenuLink;
use App\Models\Product;
use App\Models\SeoPage;
use App\Models\TopSlider;
use App\Models\ServicesItem;

class ServicesPageController extends Controller
{
  public function servicesPage()
  {
    $page = SeoPage::where('id', '=', '1')->with('translations') ->firstOrFail();

    $header_links = HeaderLink::get();
    $header_texts = HeaderText::get();
    $footer_links = FooterLink::get();
    $footer_texts = FooterText::get();

    $top_sliders = TopSlider::get();

    $products   = Product::where('popular', '=', '1')->get();

    $menu_constructor = MenuLink::get();
    $header_logo = Setting::where('key', '=', 'site.logo')->first();
    $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

    $services_items = ServicesItem::get();


    return view(
      'services.services',
      compact(
        'page',
        'header_texts',
        'header_links',
        'footer_texts',
        'footer_links',
        'top_sliders',
        'products',
        'menu_constructor',
        'services_items',
        'header_logo',
        'footer_logo'
      ));
  }

  /*public function servicesCategoryPage($lang, $slug)
  {

    $page = SeoPage::where('id', '=', '3')->with('translations')->firstOrFail();
    $serviceList = ServiceCategory::where('slug', '=', $slug)->firstOrFail();
    $serviceNew = Services::where('category_id', '=', $serviceList->id)->orderBy('id', 'desc')->limit(8)->get();


    return view('services-category', compact('page', 'serviceList', 'serviceNew'));
  }

  public function servicesDetailPage($lang, $slug)
  {

    $page = Services::with('translations')->firstOrFail();
    $serviceNew = Services::where('slug', '=', $slug)->firstOrFail();


    return view('services-detail', compact('page', 'serviceNew'));
  }*/
}

<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\SeoPage;
use App\Models\FooterLink;
use App\Models\FooterText;
use App\Models\HeaderLink;
use App\Models\HeaderText;
use App\Models\MenuLink;
use App\Models\Product;
use App\Models\TopSlider;
use App\Models\AboutPageText;
use App\Models\Experience;
use App\Models\MainPageImage;

class AboutPageController extends Controller
{
  public function aboutPage($lang)
  {
    $page = SeoPage::where('id', '=', '1')->with('translations')->firstOrFail();

    $header_links = HeaderLink::get();
    $header_texts = HeaderText::get();
    $footer_links = FooterLink::get();
    $footer_texts = FooterText::get();

    $top_sliders = TopSlider::get();

    $products = Product::where('popular', '=', '1')->get();

    $header_logo = Setting::where('key', '=', 'site.logo')->first();
    $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

    $menu_constructor = MenuLink::get();
    $texts = AboutPageText::get();
    $experiences = Experience::get();
    $about_image = MainPageImage::where('identifier', '=', 'about_page_image')->first();
    $about_image_experience = MainPageImage::where('identifier', '=', 'about_page_experience_image')->first();

    return view('about.about', compact('page',
      'header_links',
      'header_texts',
      'menu_constructor',
      'footer_links',
      'footer_texts',
      'products',
      'top_sliders',
      'texts',
      'experiences',
      'about_image',
    'about_image_experience',
    'header_logo',
    'footer_logo'
    ));
  }
}

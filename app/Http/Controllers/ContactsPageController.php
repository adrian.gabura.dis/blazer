<?php

namespace App\Http\Controllers;

use App\Models\FooterLink;
use App\Models\FooterText;
use App\Models\HeaderLink;
use App\Models\HeaderText;
use App\Models\MenuLink;
use App\Models\Product;
use App\Models\TopSlider;
use Illuminate\Http\Request;
use App\Models\SeoPage;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Mail\SendUserMail;
use App\Models\FrequentQuestion;
use App\Models\ContactUsText;


class ContactsPageController extends Controller
{
  public function contactsPage()
  {
    $page = SeoPage::where('id', '=', '1')->with('translations')->firstOrFail();

    $header_links = HeaderLink::get();
    $header_texts = HeaderText::get();
    $footer_links = FooterLink::get();
    $footer_texts = FooterText::get();

    $top_sliders = TopSlider::get();

    $products = Product::where('popular', '=', '1')->get();

    $menu_constructor = MenuLink::get();
    $header_logo = Setting::where('key', '=', 'site.logo')->first();
    $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

    $frequent_questions = FrequentQuestion::get();

    $contact_us_texts = ContactUsText::get();

    return view('contacts.contacts',
      compact(
        'page',
        'header_links',
        'header_texts',
        'footer_links',
        'footer_texts',
        'top_sliders',
        'products',
        'menu_constructor',
        'frequent_questions',
        'contact_us_texts',
        'header_logo',
        'footer_logo'
      ));
  }

  public function sendMessage(Request $request)
  {
    $email = Setting::where('key', '=', 'site.email')->firstOrFail();
    $email_to = $email['value'];

    $name = $request->input('name');
    //$name = "adrian";
    $mail = $request->input('mail');
    //$mail = "adriangabura@gmail.com";
    $phone = $request->input('phone');
    //$phone = "21321312312";
    $company = $request->input('company');
    //$company = "dasdasds";
    $mail_message = $request->input('mail_message');
    //$mail_message = "asddasdasdasdasdas";

    $mailable_blazer = new SendMail($email_to, $name, $mail, $phone, $company, $mail_message);
    $mailable_user = new SendUserMail($email_to, $name, $mail, $phone, $company, $mail_message);

    Mail::to($email_to)->send($mailable_blazer);
    Mail::to($mail)->send($mailable_user);
    //Mail::to("a.gabura@dis.agency")->send(new SendMail($email_to, $name, $mail, $phone, $company, $message));
    //Mail::send(new SendMail($email_to, $name, $mail, $phone, $company, $message));
    //Mail::send();
  }

  public function subscribe(Request $request)
  {
    $mail_s = $request->email_s;
    $name_email = Setting::where('key', 'admin.form_input_email')->first();
    $post = $name_email['value'] . '=' . $mail_s;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://docs.google.com/');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_exec($curl);
    curl_close($curl);

    return response()->json(trans('tr.js_subs_success'));
  }
}

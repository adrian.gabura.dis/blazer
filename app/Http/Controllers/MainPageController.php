<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SeoPage;
use App\Models\MenuLink;
use App\Models\Product;
use App\Models\Review;
use App\Models\Portofolio;
use App\Models\MainPageText;
use App\Models\MainPageImage;
use App\Models\FooterText;
use App\Models\FooterLink;
use App\Models\HeaderLink;
use App\Models\HeaderText;
use App\Models\TopSlider;
use App\Models\Advantage;
use App\Models\Setting;

class MainPageController extends Controller
{
    public function mainPage($lang){
        $page       = SeoPage::where('id', '=', '1')->with('translations') ->firstOrFail();

        $menu_constructor = MenuLink::get();
        $texts = MainPageText::get();
        $images = MainPageImage::get();
        $footer_texts = FooterText::get();
        $footer_links = FooterLink::get();
        $header_links = HeaderLink::get();
        $header_texts = HeaderText::get();
        $top_sliders = TopSlider::get();
        $advantages = Advantage::get();
        $header_logo = Setting::where('key', '=', 'site.logo')->first();
        $footer_logo = Setting::where('key', '=', 'site.footer_logo')->first();

        $products   = Product::where('popular', '=', '1')->get();
        $reviews    = Review::get();
        $portofolios = Portofolio::get();
        //$slider		= Slider::orderBy('id', 'desc')->get();


        //dd($information);
        return view('index', compact('page', 'products', 'reviews', 'portofolios', 'menu_constructor', 'texts', 'images', 'footer_texts', 'footer_links', 'header_links', 'header_texts', 'top_sliders', 'advantages', 'header_logo', 'footer_logo'));
    }
}

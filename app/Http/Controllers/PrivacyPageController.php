<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrivacyPageController extends Controller
{
    //
public function privacyPage(){
        $page       = SeoPage::where('id', '=', '7')->with('translations') ->firstOrFail();
        $banner     = SeoPage::where('id', '=', '7')->first();
        $privacyPage    = Privacy::first();
        $vocabulary = PageVocabulary::orderBy('id', 'asc')->get();

        return view('privacy', compact('page','privacyPage','vocabulary','banner'));
    }
}
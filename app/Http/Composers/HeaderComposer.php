<?php

namespace App\Http\Composers;


use Illuminate\View\View;
use Request;

use App\Models\SeoPage;
/**
 * Class FooterComposer.
 */
class HeaderComposer
{
   

    public function __construct() {
        
    }
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {



        $url = Request::url();
        $router = parse_url($url);
        $rest = substr($router["path"], 3, );

        $view->with([
            // 'footerServices' => Services::orderBy('id', 'desc')->limit(6)->get(),
            'menu' => SeoPage::get(),
            'rest' => $rest,
        ]);
    }
}

<?php

namespace App\Http\Composers;


use Illuminate\View\View;
//use App\Models\Services;
 use App\Models\SeoPage;
// use App\Models\Contact;

/**
 * Class FooterComposer.
 */
class FooterComposer
{
   

    public function __construct() {
        
    }
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        
        $view->with([
        //    'footerServices' => Services::orderBy('id', 'desc')->limit(6)->get(),
        //    'footerServicesCount' => Services::count()
            'menu' => SeoPage::get(),
        //    'contacts' => Contact::first(),
        ]);
    }
}

<?php

namespace App\Http\Composers;


use Illuminate\View\View;

// use App\Models\Vocabulary;

/**
 * Class AllPagesComposer.
 */
class AllPagesComposer
{
   

    public function __construct() {
        
    }
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        
        $view->with([
        //    'vocabulary' => Vocabulary::orderBy('id', 'asc')->get(),
        ]);
    }
}
